// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of util.
//
// Util is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// util is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with util.  If not, see <http://www.gnu.org/licenses/>.

#include "util.cxx"

using namespace std;
using namespace util;

int main(int argc, char *argv[])
{
  vector<int> ivector(20, 7);
  vector<double> dvector(10, double(3.1415));
  vector<string> svector1(10, "hello");
  vector<vector<string> > svector2(10, svector1);

  map<string, double> dmap = {{"a", 2.34}, {"b", 3.5812}, {"c", 9.02323}, {"d", 7.12389}};
  map<string, string> smap = {{"a", "toto"}, {"b", "tata"}, {"c", "titi"}, {"d", "tutu"}};
  map<int, int> imap = {{23, 1}, {32, 7}, {89, 1}, {238734, 9}, {786, 3}};

  cout << ivector << endl
       << dvector << endl
       << svector1 << endl
       << svector2 << endl
       << imap << endl
       << dmap << endl
       << smap << endl;

  const double d = util::convert<double>("54.23454");
  cout << d << endl;

  const double e = 85672.523123;
  cout << "e = " + to_str(e) << endl;

  try
    {
      const int i = util::convert<int>("s3.405");
      cout << i << endl;
    }
  catch (std::string& str)
    {
      cout << str << endl;
    }

  try
    {
      const int i = util::convert<int>("7.902");
      cout << i << endl;
    }
  catch (std::string& str)
    {
      cout << str << endl;
    }

  try
    {
      const float f = util::convert<float>("g 7.902");
      cout << f << endl;
    }
  catch (std::string& str)
    {
      cout << str << endl;
    }


  try
    {
      make_directory("./tata/toto");
      make_directory("./tata/toto/titi/");
      make_directory("./tata/toto/titi/tete");
      make_directory("/tata");
    }
  catch (std::string& str)
    {
      cout << str << endl;
    }

  try
    {
      check_directory("./tata/toto");
      check_directory("./tata/toto/titi/");
      check_directory("./tata/toto/titi/tete");
      check_directory("./tata/toto/titi/tete/tutu");
    }
  catch (std::string& str)
    {
      cout << str << endl;
    }

  try
    {
      check_file("./example.txt");
    }
  catch (std::string& str)
    {
      cout << str << endl;
    }

  try
    {
      std::ofstream fout("./example.txt");
      fout << std::string(100, 'a');
      fout.close();
      check_file("./example.txt");
    }
  catch (std::string& str)
    {
      cout << str << endl;
    }

  try
    {
      std::ofstream fout("./example.txt");
      fout.close();
      check_file("./example.txt");
    }
  catch (std::string& str)
    {
      cout << str << endl;
    }

  try
    {
      cout << "Current date : \"" << get_current_time() << "\"." << endl;
      cout << "Current date : \"" << get_current_time("%c %Z") << "\"." << endl;
      cout << "Current date : \"" << get_current_time("%a, %d %b %Y %H:%M:%S %z") << "\"." << endl;
      cout << "User name : \"" << get_user_name() << "\"." << endl;
      cout << "Host/Domain name : \"" << get_host_name() << "." << get_domain_name() << "\"." << endl;
      cout << "Number of cpu : " << get_cpu_number() << endl;
      cout << "Memory usage : " << get_memory_usage() << endl;
    }
  catch (std::string& str)
    {
      cout << str << endl;
    }

  return 0;
}
