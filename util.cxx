// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of util.
//
// Util is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Util is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Util.  If not, see <http://www.gnu.org/licenses/>.

#ifndef UTIL_FILE_UTIL_CXX

#include "util.hxx"

namespace util
{
  // Change everything into a string.
  template<typename T>
  std::string to_str(T value)
  {
    std::ostringstream oss;
    oss << value;
    return oss.str();
  }


  // Convert a string in a given type.
  template<typename T>
  T convert(const std::string &str)
  {
    T value;

    std::istringstream iss(str);
    iss >> value;

    if (iss.fail())
      throw std::string("Failed to convert \"" + str + "\" to type \"" + std::string(typeid(T).name()) + "\".");

    return value;
  }


  // Check file path.
  void check_file(const std::string path)
  {
    std::ifstream fin(path.c_str(), std::ifstream::ate | std::ifstream::binary);

    if (! fin.good())
      throw std::string("Unable to open path \"" + path + "\" for reading.");

    if (fin.tellg() == 0)
      throw std::string("Path \"" + path + "\" is empty.");

    fin.close();
  }


  // Check directory path.
  void check_directory(const char *path)
  {
    char *current_work_directory = get_current_dir_name();

    if (chdir(path) != 0)
      throw std::string("Unable to open directory \"" + std::string(path) + "\".");

    int ignore_value = chdir(current_work_directory);
    free(current_work_directory);
  }


  // Make directory.
  void make_directory(const std::string path, mode_t mode)
  {
    size_t pos(0);

    while (pos < path.size())
      {
        pos = path.find_first_of('/', pos + 1);
        const std::string sub = path.substr(0, pos);

        if (mkdir(sub.c_str(), mode) < 0 && errno != EEXIST)
          throw std::string("Unable to make subdirectory \"" + sub + "\" : " + std::string(strerror(errno)));
      }
  }


  // Get number of CPU.
  int get_cpu_number(const int ncpu_max)
  {
    int ncpu(0);

#ifdef __linux__
    cpu_set_t cs;
    CPU_ZERO(&cs);
    sched_getaffinity(0, sizeof(cs), &cs);

    for (int i = 0; i < ncpu_max; ++i)
      if (CPU_ISSET(i, &cs))
        ncpu++;
#else
    throw std::string(__FUNCTION__) + std::string(" is not yet implemented in this operating system.");
#endif

    return ncpu;
  }


  // Retrieve memory usage.
  double get_memory_usage()
  {
#ifdef __linux__
    struct sysinfo info;

    if (sysinfo(&info) > 0)
      throw std::string("Bad return state with sysinfo().");

    return double(info.freeram) / double(info.totalram);
#else
    throw std::string(__FUNCTION__) + std::string(" is not yet implemented in this operating system.");
#endif
  }


  // Get current time.
  std::string get_current_time(const std::string format)
  {
    time_t t;
    time(&t);
    struct tm *tmp = localtime(&t);
    char cstr[200];
    strftime(cstr, sizeof(cstr), format.c_str(), tmp);
    return std::string(cstr);
  }


  // Get user name.
  std::string get_user_name()
  {
#ifdef __linux__
    uid_t uid = geteuid();
    struct passwd *pw = getpwuid(uid);

    std::ostringstream name;

    if (pw)
      name << pw->pw_name;
    else
      name << uid;

    endpwent();

    return name.str();
#else
    throw std::string(__FUNCTION__) + std::string(" is not yet implemented in this operating system.");
#endif
  }


  // Get host name.
  std::string get_host_name()
  {
#ifdef __linux__
    std::ifstream fin("/proc/sys/kernel/hostname");

    if (! fin.good())
      throw std::string("Could not open \"/proc/sys/kernel/hostname\" for reading.");

    std::string name(std::istreambuf_iterator<char>(fin), {});

    fin.close();

    if (name.back() == '\n')
      name.erase(name.end() - 1);

    return name;
#else
    throw std::string(__FUNCTION__) + std::string(" is not yet implemented in this operating system.");
#endif
  }


  // Get domain name.
  std::string get_domain_name()
  {
#ifdef __linux__
    std::ifstream fin("/proc/sys/kernel/domainname");

    if (! fin.good())
      throw std::string("Could not open \"/proc/sys/kernel/domainname\" for reading.");

    std::string name(std::istreambuf_iterator<char>(fin), {});

    fin.close();

    if (name.back() == '\n')
      name.erase(name.end() - 1);

    return name;
#else
    throw std::string(__FUNCTION__) + std::string(" is not yet implemented in this operating system.");
#endif
  }


  // Display C++ vectors.
  template<typename T>
  std::ostream& operator << (std::ostream& os, const std::vector<T> &v)
  {
    const int n = int(v.size()) - 1;
    os << '[';
    for (int i = 0; i < n; i++)
      os << v[i] << ", ";
    if (n >= 0)
      os << v.back();
    os << ']';

    return os;
  }


  template<typename T>
  std::ostream& operator << (std::ostream& os, const std::vector<std::vector<T> > &v)
  {
    const int n = int(v.size()) - 1;
    os << '[';
    for (int i = 0; i < n; i++)
      os << v[i] << ", ";
    if (n >= 0)
      os << v.back();
    os << ']';

    return os;
  }


  std::ostream& operator << (std::ostream& os, const std::vector<std::string> &v)
  {
    const int n = int(v.size()) - 1;
    os << '[';
    for (int i = 0; i < n; i++)
      os << '\"' << v[i] << "\", ";
    if (n >= 0)
      os << '\"' << v.back() << '\"';
    os << ']';

    return os;
  }


  std::ostream& operator << (std::ostream& os, const std::vector<std::vector<std::string> > &v)
  {
    const int n = int(v.size()) - 1;
    os << "[ ";
    if (n >= 0)
      os << v.front() << ",\n";
    for (int i = 1; i < n; i++)
      os << "  " << v[i] << ",\n";
    if (n >= 0)
      os << "  " << v.back();
    os << " ]";

    return os;
  }


  // Display C++ maps.
  template<typename T>
  std::ostream& operator << (std::ostream& os, const std::map<std::string, T> &m)
  {
    os << '{';
    if (m.begin() != m.end())
      {
        typename std::map<std::string, T>::const_iterator beg = m.begin();
        typename std::map<std::string, T>::const_iterator end = --m.end();
        for (typename std::map<std::string, T>::const_iterator it = beg; it != end; it++)
          os << '\"' << it->first << "\" : " << it->second << ", ";
        os << '\"' << end->first << "\" : " << end->second;
      }
    os << '}';

    return os;
  }


  std::ostream& operator << (std::ostream& os, const std::map<std::string, std::string> &m)
  {
    os << '{';
    if (m.begin() != m.end())
      {
        std::map<std::string, std::string>::const_iterator beg = m.begin();
        std::map<std::string, std::string>::const_iterator end = --m.end();
        for (std::map<std::string, std::string>::const_iterator it = beg; it != end; it++)
          os << '\"' << it->first << "\" : \"" << it->second << "\", ";
        os << '\"' << end->first << "\" : \"" << end->second << '\"';
      }
    os << '}';

    return os;
  }


  std::ostream& operator << (std::ostream& os, const std::map<int, int> &m)
  {
    os << '{';
    if (m.begin() != m.end())
      {
        std::map<int, int>::const_iterator beg = m.begin();
        std::map<int, int>::const_iterator end = --m.end();
        for (std::map<int, int>::const_iterator it = beg; it != end; it++)
          os << it->first << " : " << it->second << ", ";
        os << end->first << " : " << end->second;
      }
    os << '}';

    return os;
  }
}

#define UTIL_FILE_UTIL_CXX
#endif
