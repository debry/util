#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Edouard Debry
# Author(s) : Edouard Debry
# 
# This file is part of Util.
# 
# Util is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Util is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Util.  If not, see <http://www.gnu.org/licenses/>.

import os, sys
import glob, re

old_str = sys.argv[1]
new_str = sys.argv[2]

if len(sys.argv[3:]) > 0:
    files = sys.argv[3:]
else:
    files = glob.glob("*.cpp") + glob.glob("*.?xx") \
        + glob.glob("*.i") + glob.glob("*/*.py")


print "Replace \"%s\" with \"%s\" in files:" % (old_str, new_str)
for f in files:
    print "\t%s" % f
print "Go ?"
try:
    raw_input()
except KeyboardInterrupt:
    print "Aborting, NO files modified."
    sys.exit(0)

for f in files:
    str_file = open(f).read()

    str_file = str_file.replace(old_str, new_str)

    open(f, "w").write(str_file)
