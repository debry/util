// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of util.
//
// Util is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Util is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Util.  If not, see <http://www.gnu.org/licenses/>.

#ifndef UTIL_FILE_UTIL_HXX

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <pwd.h>
#include <unistd.h>
#include <cerrno>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <typeinfo>
#include <string>
#include <vector>
#include <map>

#ifdef DISP
#undef DISP
#endif
#define DISP(x) std::cout << #x ": " << x << std::endl

#ifdef ERR
#undef ERR
#endif
#define ERR(x) std::cout << "Hermes - " #x << std::endl

namespace util
{
  /*!< Change everything into a string.*/
  template<typename T>
  std::string to_str(T value);

  /*!< Convert a string in a given type.*/
  template<typename T>
  T convert(const std::string &str);

  /*!< Check file.*/
  void check_file(const std::string path);

  /*!< Check directory.*/
  void check_directory(const char *path);

  /*!< Make directory.*/
  void make_directory(const std::string path, mode_t mode = 0755);

  /*!< Get number of CPU.*/
  int get_cpu_number(const int ncpu_max = 128);

  /*!< Retrieve memory usage.*/
  double get_memory_usage();

  /*!< Get current time.*/
  std::string get_current_time(const std::string format = "%F_%H-%M-%S");

  /*!< Get user name.*/
  std::string get_user_name();

  /*!< Get host name.*/
  std::string get_host_name();

  /*!< Get domain name.*/
  std::string get_domain_name();

  /*!< The sequel is only intended for core C++ programs.*/
#ifndef SWIG
  /*!< Display C++ vectors.*/
  template<typename T>
  std::ostream& operator << (std::ostream& os, const std::vector<T> &v);

  template<typename T>
  std::ostream& operator << (std::ostream& os, const std::vector<std::vector<T> > &v);

  std::ostream& operator << (std::ostream& os, const std::vector<std::string> &v);
  std::ostream& operator << (std::ostream& os, const std::vector<std::vector<std::string> > &v);

  /*!< Display C++ maps.*/
  template<typename T>
  std::ostream& operator << (std::ostream& os, const std::map<std::string, T> &m);

  std::ostream& operator << (std::ostream& os, const std::map<std::string, std::string> &m);
  std::ostream& operator << (std::ostream& os, const std::map<int, int> &m);
#endif
}

#define UTIL_FILE_UTIL_HXX
#endif
